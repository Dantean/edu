package com.qfedu.superedu.controller;

import com.qfedu.superedu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.qfedu.superedu.entity.TbStudent;
import com.qfedu.superedu.service.intf.TbStudentService;
/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@RestController
@RequestMapping("/api/tbStudent")
public class TbStudentController {
    @Autowired
    private TbStudentService tbStudentService;
    //新增
    @PostMapping("/add.do")
    public R save(@RequestBody TbStudent tbStudent){
        return tbStudentService.save(tbStudent);
    }
    //删除
    @DeleteMapping("/del.do")
    public R del(int id){
        return tbStudentService.delById(id);
    }
    //查询
    @GetMapping("/all.do")
    public R all(){
        return tbStudentService.all();
    }
}