package com.qfedu.superedu.controller;

import com.qfedu.superedu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.qfedu.superedu.entity.TbVideo;
import com.qfedu.superedu.service.intf.TbVideoService;
/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@RestController
@RequestMapping("/api/tbVideo")
public class TbVideoController {
    @Autowired
    private TbVideoService tbVideoService;
    //新增
    @PostMapping("/add.do")
    public R save(@RequestBody TbVideo tbVideo){
        return tbVideoService.save(tbVideo);
    }
    //删除
    @DeleteMapping("/del.do")
    public R del(int id){
        return tbVideoService.delById(id);
    }
    //查询
    @GetMapping("/all.do")
    public R all(){
        return tbVideoService.all();
    }
}