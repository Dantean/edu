package com.qfedu.superedu.controller;

import com.qfedu.superedu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.qfedu.superedu.entity.TbSign;
import com.qfedu.superedu.service.intf.TbSignService;
/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@RestController
@RequestMapping("/api/tbSign")
public class TbSignController {
    @Autowired
    private TbSignService tbSignService;
    //新增
    @PostMapping("/add.do")
    public R save(@RequestBody TbSign tbSign){
        return tbSignService.save(tbSign);
    }
    //删除
    @DeleteMapping("/del.do")
    public R del(int id){
        return tbSignService.delById(id);
    }
    //查询
    @GetMapping("/all.do")
    public R all(){
        return tbSignService.all();
    }
}