package com.qfedu.superedu.controller;

import com.qfedu.superedu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.qfedu.superedu.entity.TbTeacher;
import com.qfedu.superedu.service.intf.TbTeacherService;
/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@RestController
@RequestMapping("/api/tbTeacher")
public class TbTeacherController {
    @Autowired
    private TbTeacherService tbTeacherService;
    //新增
    @PostMapping("/add.do")
    public R save(@RequestBody TbTeacher tbTeacher){
        return tbTeacherService.save(tbTeacher);
    }
    //删除
    @DeleteMapping("/del.do")
    public R del(int id){
        return tbTeacherService.delById(id);
    }
    //查询
    @GetMapping("/all.do")
    public R all(){
        return tbTeacherService.all();
    }
}