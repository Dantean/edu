package com.qfedu.superedu.controller;

import com.qfedu.superedu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.qfedu.superedu.entity.TbTemp;
import com.qfedu.superedu.service.intf.TbTempService;
/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@RestController
@RequestMapping("/api/tbTemp")
public class TbTempController {
    @Autowired
    private TbTempService tbTempService;
    //新增
    @PostMapping("/add.do")
    public R save(@RequestBody TbTemp tbTemp){
        return tbTempService.save(tbTemp);
    }
    //删除
    @DeleteMapping("/del.do")
    public R del(int id){
        return tbTempService.delById(id);
    }
    //查询
    @GetMapping("/all.do")
    public R all(){
        return tbTempService.all();
    }
}