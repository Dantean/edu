package com.qfedu.superedu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qfedu.superedu.vo.R;
import com.qfedu.superedu.entity.TbPaper;
import com.qfedu.superedu.dao.TbPaperDao;
import com.qfedu.superedu.service.intf.TbPaperService;

/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@Service
public class TbPaperServiceImpl implements TbPaperService{

    @Autowired
    private TbPaperDao tbPaperDao;

    @Override
    public R save(TbPaper tbPaper){
        if(tbPaperDao.insert(tbPaper)>0){
            return R.ok();
        }else{
            return R.fail("新增失败");
        }
    }
    @Override
    public R delById(int id){
        if(tbPaperDao.deleteById(id)>0){
            return R.ok();
        }else{
            return R.fail("删除失败");
        }
    }
    @Override
    public R all(){
       return R.ok(tbPaperDao.all());
    }
}
