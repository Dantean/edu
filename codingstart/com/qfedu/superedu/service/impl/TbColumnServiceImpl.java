package com.qfedu.superedu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qfedu.superedu.vo.R;
import com.qfedu.superedu.entity.TbColumn;
import com.qfedu.superedu.dao.TbColumnDao;
import com.qfedu.superedu.service.intf.TbColumnService;

/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@Service
public class TbColumnServiceImpl implements TbColumnService{

    @Autowired
    private TbColumnDao tbColumnDao;

    @Override
    public R save(TbColumn tbColumn){
        if(tbColumnDao.insert(tbColumn)>0){
            return R.ok();
        }else{
            return R.fail("新增失败");
        }
    }
    @Override
    public R delById(int id){
        if(tbColumnDao.deleteById(id)>0){
            return R.ok();
        }else{
            return R.fail("删除失败");
        }
    }
    @Override
    public R all(){
       return R.ok(tbColumnDao.all());
    }
}
