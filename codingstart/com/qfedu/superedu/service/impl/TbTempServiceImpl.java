package com.qfedu.superedu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qfedu.superedu.vo.R;
import com.qfedu.superedu.entity.TbTemp;
import com.qfedu.superedu.dao.TbTempDao;
import com.qfedu.superedu.service.intf.TbTempService;

/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@Service
public class TbTempServiceImpl implements TbTempService{

    @Autowired
    private TbTempDao tbTempDao;

    @Override
    public R save(TbTemp tbTemp){
        if(tbTempDao.insert(tbTemp)>0){
            return R.ok();
        }else{
            return R.fail("新增失败");
        }
    }
    @Override
    public R delById(int id){
        if(tbTempDao.deleteById(id)>0){
            return R.ok();
        }else{
            return R.fail("删除失败");
        }
    }
    @Override
    public R all(){
       return R.ok(tbTempDao.all());
    }
}
