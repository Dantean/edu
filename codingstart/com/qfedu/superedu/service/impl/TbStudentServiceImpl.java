package com.qfedu.superedu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qfedu.superedu.vo.R;
import com.qfedu.superedu.entity.TbStudent;
import com.qfedu.superedu.dao.TbStudentDao;
import com.qfedu.superedu.service.intf.TbStudentService;

/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@Service
public class TbStudentServiceImpl implements TbStudentService{

    @Autowired
    private TbStudentDao tbStudentDao;

    @Override
    public R save(TbStudent tbStudent){
        if(tbStudentDao.insert(tbStudent)>0){
            return R.ok();
        }else{
            return R.fail("新增失败");
        }
    }
    @Override
    public R delById(int id){
        if(tbStudentDao.deleteById(id)>0){
            return R.ok();
        }else{
            return R.fail("删除失败");
        }
    }
    @Override
    public R all(){
       return R.ok(tbStudentDao.all());
    }
}
