package com.qfedu.superedu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qfedu.superedu.vo.R;
import com.qfedu.superedu.entity.TbSign;
import com.qfedu.superedu.dao.TbSignDao;
import com.qfedu.superedu.service.intf.TbSignService;

/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@Service
public class TbSignServiceImpl implements TbSignService{

    @Autowired
    private TbSignDao tbSignDao;

    @Override
    public R save(TbSign tbSign){
        if(tbSignDao.insert(tbSign)>0){
            return R.ok();
        }else{
            return R.fail("新增失败");
        }
    }
    @Override
    public R delById(int id){
        if(tbSignDao.deleteById(id)>0){
            return R.ok();
        }else{
            return R.fail("删除失败");
        }
    }
    @Override
    public R all(){
       return R.ok(tbSignDao.all());
    }
}
