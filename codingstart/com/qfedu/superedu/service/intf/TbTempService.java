package com.qfedu.superedu.service.intf;

import com.qfedu.superedu.vo.R;
import com.qfedu.superedu.entity.TbTemp;

/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
public interface TbTempService {
    /*新增*/
    R save(TbTemp tbTemp);
    /*删除*/
    R delById(int id);
    /*查询全部*/
    R all();
}