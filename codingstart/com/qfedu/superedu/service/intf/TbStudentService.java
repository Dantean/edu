package com.qfedu.superedu.service.intf;

import com.qfedu.superedu.vo.R;
import com.qfedu.superedu.entity.TbStudent;

/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
public interface TbStudentService {
    /*新增*/
    R save(TbStudent tbStudent);
    /*删除*/
    R delById(int id);
    /*查询全部*/
    R all();
}