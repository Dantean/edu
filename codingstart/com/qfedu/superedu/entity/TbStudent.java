package com.qfedu.superedu.entity;

import lombok.Data;
import java.util.Date;

/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@Data
public class TbStudent {
    private Integer id;
    private String user_name;
    private String password;
    private String name;
    private String phone;
    private Date birthday;
    private Integer score;
    private String sex;
}