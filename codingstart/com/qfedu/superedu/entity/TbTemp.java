package com.qfedu.superedu.entity;

import lombok.Data;
import java.util.Date;

/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@Data
public class TbTemp {
    private Integer id;
    private Integer pid;
    private Integer qid;
}