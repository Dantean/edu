package com.qfedu.superedu.entity;

import lombok.Data;
import java.util.Date;

/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@Data
public class TbOrder {
    private Integer id;
    private Date create_time;
    private Integer sid;
    private Double price;
    private Integer vid;
    private Integer tid;
    private String status;
}