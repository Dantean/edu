package com.qfedu.superedu.entity;

import lombok.Data;
import java.util.Date;

/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@Data
public class TbVideo {
    private Integer id;
    private String name;
    private String vurl;
    private String iurl;
    private Integer tid;
    private String audit;
    private Double oprice;
    private Double nprice;
    private String flag;
    private Integer pay_count;
}