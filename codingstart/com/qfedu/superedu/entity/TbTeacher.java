package com.qfedu.superedu.entity;

import lombok.Data;
import java.util.Date;

/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@Data
public class TbTeacher {
    private Integer id;
    private String user_name;
    private String password;
    private String name;
    private Date birthday;
    private String phone;
    private String sex;
    private String graduation_school;
    private String id_card;
    private String certificate_code;
    private Integer cid;
    private String birth_area;
    private String remark;
    private String imgpath;
    private Integer workyear;
}