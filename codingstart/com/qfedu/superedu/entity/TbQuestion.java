package com.qfedu.superedu.entity;

import lombok.Data;
import java.util.Date;

/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
@Data
public class TbQuestion {
    private Integer id;
    private String name;
    private String optionA;
    private String optionB;
    private String optionC;
    private String optionD;
    private String answer;
    private Integer count;
    private Integer right_count;
}