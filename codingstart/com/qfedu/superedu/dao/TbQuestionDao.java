package com.qfedu.superedu.dao;

import com.qfedu.superedu.entity.TbQuestion;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;



/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
public interface TbQuestionDao {
    /*新增*/
    @Insert("insert into tb_question (id,name,optionA,optionB,optionC,optionD,answer,count,right_count) values(#{id},#{name},#{optionA},#{optionB},#{optionC},#{optionD},#{answer},#{count},#{right_count})")
    int insert(TbQuestion tbQuestion);

    /*删除*/
    @Delete("delete from tb_question where id=#{id}")
    int deleteById(int id);
    /*查询全部*/
    @Select("select * from tb_question")
    TbQuestion all();
}