package com.qfedu.superedu.dao;

import com.qfedu.superedu.entity.TbTeacher;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;



/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
public interface TbTeacherDao {
    /*新增*/
    @Insert("insert into tb_teacher (id,user_name,password,name,birthday,phone,sex,graduation_school,id_card,certificate_code,cid,birth_area,remark,imgpath,workyear) values(#{id},#{user_name},#{password},#{name},#{birthday},#{phone},#{sex},#{graduation_school},#{id_card},#{certificate_code},#{cid},#{birth_area},#{remark},#{imgpath},#{workyear})")
    int insert(TbTeacher tbTeacher);

    /*删除*/
    @Delete("delete from tb_teacher where id=#{id}")
    int deleteById(int id);
    /*查询全部*/
    @Select("select * from tb_teacher")
    TbTeacher all();
}