package com.qfedu.superedu.dao;

import com.qfedu.superedu.entity.TbStudent;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;



/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
public interface TbStudentDao {
    /*新增*/
    @Insert("insert into tb_student (id,user_name,password,name,phone,birthday,score,sex) values(#{id},#{user_name},#{password},#{name},#{phone},#{birthday},#{score},#{sex})")
    int insert(TbStudent tbStudent);

    /*删除*/
    @Delete("delete from tb_student where id=#{id}")
    int deleteById(int id);
    /*查询全部*/
    @Select("select * from tb_student")
    TbStudent all();
}