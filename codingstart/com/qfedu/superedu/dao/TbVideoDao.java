package com.qfedu.superedu.dao;

import com.qfedu.superedu.entity.TbVideo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;



/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
public interface TbVideoDao {
    /*新增*/
    @Insert("insert into tb_video (id,name,vurl,iurl,tid,audit,oprice,nprice,flag,pay_count) values(#{id},#{name},#{vurl},#{iurl},#{tid},#{audit},#{oprice},#{nprice},#{flag},#{pay_count})")
    int insert(TbVideo tbVideo);

    /*删除*/
    @Delete("delete from tb_video where id=#{id}")
    int deleteById(int id);
    /*查询全部*/
    @Select("select * from tb_video")
    TbVideo all();
}