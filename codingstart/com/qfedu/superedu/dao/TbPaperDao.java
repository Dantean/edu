package com.qfedu.superedu.dao;

import com.qfedu.superedu.entity.TbPaper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;



/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
public interface TbPaperDao {
    /*新增*/
    @Insert("insert into tb_paper (id,tid,name,create_date) values(#{id},#{tid},#{name},#{create_date})")
    int insert(TbPaper tbPaper);

    /*删除*/
    @Delete("delete from tb_paper where id=#{id}")
    int deleteById(int id);
    /*查询全部*/
    @Select("select * from tb_paper")
    TbPaper all();
}