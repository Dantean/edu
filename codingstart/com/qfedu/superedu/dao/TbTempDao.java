package com.qfedu.superedu.dao;

import com.qfedu.superedu.entity.TbTemp;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;



/**
 * @description: 自动生成香不香
 * @author: 旭阳
 * @create: 2020-09-15 21:53:16
 */
public interface TbTempDao {
    /*新增*/
    @Insert("insert into tb_temp (id,pid,qid) values(#{id},#{pid},#{qid})")
    int insert(TbTemp tbTemp);

    /*删除*/
    @Delete("delete from tb_temp where id=#{id}")
    int deleteById(int id);
    /*查询全部*/
    @Select("select * from tb_temp")
    TbTemp all();
}