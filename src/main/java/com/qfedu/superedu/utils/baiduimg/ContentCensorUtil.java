package com.qfedu.superedu.utils.baiduimg;

import com.baidu.aip.contentcensor.AipContentCensor;
import org.json.JSONObject;

public class ContentCensorUtil {
    //设置APPID/AK/SK
    public static final String APP_ID = "22674676";
    public static final String API_KEY = "UduNE6Y0rN3sHepOYRXIw9Uu";
    public static final String SECRET_KEY = "MGDI98DXi7ox48H10BICrmabGhk4GYWN";
    private static AipContentCensor client;
    static{

        // 初始化一个AipContentCensor
        client = new AipContentCensor(APP_ID, API_KEY, SECRET_KEY);
        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);
    }

    //    审核文本内容
    public static boolean censorText(String msg) {
        JSONObject object = client.textCensorUserDefined(msg);
        if (object != null) {
            System.out.println(object);
            if (object.getInt("conclusionType") == 1) {
                return true;
            }
        }
        return false;
    }

    //    审核图片
    public static boolean censorImg(byte[] data) {
        JSONObject object = client.imageCensorUserDefined(data, null);
        if (object != null) {
            System.out.println(object);
            if (object.getInt("conclusionType") == 1) {
                return true;
            }
        }
        return false;
    }
}

