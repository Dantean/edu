package com.qfedu.superedu.utils.baiduimg;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * @program: CodingsSart
 * @description: 请求第三方接口
 * @author: Feri(邢朋辉)
 * @create: 2020-09-16 10:19
 */
public class HttpUtil {
    //get请求
    public static String get(String url){
        try {
            //1.创建连接对象
            URL u=new URL(url);
            //2.获取客户端对象
            HttpURLConnection connection=(HttpURLConnection)u.openConnection();
            //3.设置信息
            connection.setConnectTimeout(4000);
            connection.setRequestMethod("GET");
            //4.验证响应结果吗
            if(connection.getResponseCode()==200){
                //5.获取响应结果
                InputStream is=connection.getInputStream();
                String res=IOUtils.toString(is);
                //6.关闭和释放
                is.close();
                connection.disconnect();
                return res;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    //get请求 下载资源
    public static byte[] getData(String url){
        try {
            //1.创建连接对象
            URL u=new URL(url);
            //2.获取客户端对象
            HttpURLConnection connection=(HttpURLConnection)u.openConnection();
            //3.设置信息
            connection.setConnectTimeout(4000);
            connection.setRequestMethod("GET");
            //4.验证响应结果吗
            if(connection.getResponseCode()==200){
                //5.获取响应结果
                InputStream is=connection.getInputStream();
                byte[] data=IOUtils.toByteArray(is);
                //6.关闭和释放
                is.close();
                connection.disconnect();
                return data;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    //post请求
    public static String post(String url,String params){
        try {
            //1.创建连接对象
            URL u=new URL(url);
            //2.获取客户端对象
            HttpURLConnection connection=(HttpURLConnection)u.openConnection();
            //3.设置信息
            connection.setConnectTimeout(4000);
            connection.setRequestMethod("POST");
            //设置post的传递的参数
            connection.setDoOutput(true);
            connection.getOutputStream().write(params.getBytes());
            //4.验证响应结果吗
            if(connection.getResponseCode()==200){
                //5.获取响应结果
                InputStream is=connection.getInputStream();
                String res=IOUtils.toString(is);
                //6.关闭和释放
                is.close();
                connection.disconnect();
                return res;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 发起身份证识别
     * @param requestUrl 请求的url地址
     * @param accessToken 百度的授权令牌
     * @param params 请求的携带参数：身份证的正面或反面和身份证的内容（base64）*/
    public static String post(String requestUrl, String accessToken, String params)
    {
        try{
            URL url = new URL(requestUrl);
            // 打开和URL之间的连接
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            // 设置通用的请求属性
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setDoOutput(true);

            // 得到请求的输出流对象
            DataOutputStream out = new DataOutputStream(connection.getOutputStream());
            out.write(("?access_token=" + accessToken+"&"+params).getBytes("UTF-8"));
            out.flush();
            out.close();
            // 建立实际的连接
            connection.connect();
            InputStream in = connection.getInputStream();
            String result = IOUtils.toString(in);
            in.close();
            connection.disconnect();
            return result;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
