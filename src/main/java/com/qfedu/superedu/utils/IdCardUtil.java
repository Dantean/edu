package com.qfedu.superedu.utils;

import com.baidu.aip.util.Base64Util;
import org.json.JSONObject;
import com.qfedu.superedu.utils.baiduimg.HttpUtil;

import java.net.URLEncoder;

/**
 * 基于百度智能云 实现身份证识别
 */
public class IdCardUtil {

    // 官网获取的 API Key 更新为你注册的
    private static String clientId = "s2HgmRzHVck46yfT4CawuydN";
    // 官网获取的 Secret Key 更新为你注册的
    private static String clientSecret = "KG0N3uMoOLrzqFSd7TXDOcfhxc8LT39W";

    //    获取百度智能云令牌
//    有效期一个月
    public static String getToken() {
        // 获取token地址
        String authHost = "https://aip.baidubce.com/oauth/2.0/token?";
        String getAccessTokenUrl = authHost
                // 1. grant_type为固定参数
                + "grant_type=client_credentials"
                // 2. 官网获取的 API Key
                + "&client_id=" + clientId
                // 3. 官网获取的 Secret Key
                + "&client_secret=" + clientSecret;
        String result = HttpUtil.post(authHost, getAccessTokenUrl);
        if (StringUtil.isnoEmpty(result)) {
            JSONObject jsonObject = new JSONObject(result);
            return jsonObject.getString("access_token");
        }
        return null;
    }

    /**
     * 识别身份证
     *
     * @param u 要识别的身份证图片的路径地址
     * @return
     */
    public static String getIdcard(String u, String side, String accessToken) {
        // 请求url
        String url = "https://aip.baidubce.com/rest/2.0/ocr/v1/idcard";
        try {
            //下载要识别的图片
            byte[] imgData = HttpUtil.getData(u);
            //将图片内容转换为base64格式的字符串
            String imgStr = Base64Util.encode(imgData);
            //对字符串 进行编码转换
            String imgParam = URLEncoder.encode(imgStr, "UTF-8");
            //百度云 需要的参数信息
            String param = "id_card_side=" + side + "&image=" + imgParam;
            // 注意这里仅为了简化编码每一次请求都去获取access_token，线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
            String result = HttpUtil.post(url, accessToken, param);
            System.out.println(result);
            //校验有没有内容
            if (StringUtil.isnoEmpty(result)) {
                JSONObject object = new JSONObject(result);
                if (object.getString("image_status").equals("normal")) {
                    //识别成功
                    //获取识别的姓名
                    return object.getJSONObject("words_result").
                            getJSONObject("姓名").getString("words");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
