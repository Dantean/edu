package com.qfedu.superedu.qrcode;

public class QrCodeTest {

    public static void main(String[] args) throws Exception {
        // 存放在二维码中的内容
        String text = "https://docs.qq.com/sheet/DRUp2S29hRkZnSWxj?groupUin=7SG68Y057RjeKo4CnxppFA%253D%253D&ADUIN=517467778&ADSESSION=1600041372&ADTAG=CLIENT.QQ.5761_.0&ADPUBNO=27041&tab=cdkvh4";
        // 嵌入二维码的图片路径
        String imgPath = "C:\\Users\\51746\\Desktop\\test\\3.jpg";
        // 生成的二维码的路径及名称
        String destPath = "C:\\Users\\51746\\Desktop\\test\\testImg.jpg";
        //生成二维码
        QRCodeUtil.encode(text, imgPath, destPath, true);
        // 解析二维码
        String str = QRCodeUtil.decode(destPath);
        // 打印出解析出的内容
        System.out.println(str);

    }

}
