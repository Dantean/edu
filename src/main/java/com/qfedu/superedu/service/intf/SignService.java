package com.qfedu.superedu.service.intf;

import com.qfedu.superedu.entity.Sign;

import java.util.List;

public interface SignService {
    List<Sign> studentSign();
}
