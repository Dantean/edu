package com.qfedu.superedu.service.intf;

import com.qfedu.superedu.dto.CodeDto;
import com.qfedu.superedu.vo.R;

public interface CodeService {
    R create(CodeDto dto);
}
