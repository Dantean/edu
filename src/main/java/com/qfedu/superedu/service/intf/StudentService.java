package com.qfedu.superedu.service.intf;

import com.qfedu.superedu.entity.Student;
import com.qfedu.superedu.vo.R;
import org.springframework.web.multipart.MultipartFile;

public interface StudentService {

    R regisStudent(Student student, String code);

    String selectByPhone(String phone);

    String selectByUserName(String userName);

    //    登录
    R login(String phone, String pass);
    /**
     * 实现文件上传
     */
    R upload(MultipartFile file);

//        实现身份证审核
    R checkIdcrad(String token);
}
