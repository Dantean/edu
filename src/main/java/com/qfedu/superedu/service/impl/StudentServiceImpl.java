package com.qfedu.superedu.service.impl;

import com.qfedu.superedu.dao.StudentDao;
import com.qfedu.superedu.entity.Student;
import com.qfedu.superedu.redis.RedissonUtil;
import com.qfedu.superedu.service.intf.StudentService;
import com.qfedu.superedu.utils.*;
import com.qfedu.superedu.utils.baiduimg.ContentCensorUtil;
import com.qfedu.superedu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDao studentDao;
    @Value("${codingstart.rsa.prikey}")
    private String prikey;

    /**
     * @param student 学生对象
     * @param code    验证码
     * @return
     */
    @Override
    public R regisStudent(Student student, String code) {
//        密码使用密文 RSA
        student.setPassword(EncryptUtil.rsaEnc(prikey, student.getPassword()));

        if (code.equals(RedissonUtil.getStr("edu:" + student.getPhone()).toString())) {

            studentDao.regisStudent(student);
            return R.ok();
        } else {
            return R.fail();
        }
    }

    /**
     * @param phone 用户手机号
     * @return 返回注册信息
     */
    @Override
    public String selectByPhone(String phone) {
        Student student = studentDao.selectByPhone(phone);

        if (student == null) {
            SmsServiceImpl smsService = new SmsServiceImpl();
            smsService.SmsVer(phone);
            return "手机号很nice";
        } else {
            return "手机号已注册";
        }
    }

    /**
     * @param userName 用户名
     * @return 返回注册信息
     */
    @Override
    public String selectByUserName(String userName) {
        Student student = studentDao.selectByUserName(userName);
        System.out.println(student + "-----");
        if (student == null) {
            return "用户名很好听";
        } else {
            return "用户用已经被注册了哦亲";
        }
    }

    @Override
    public R login(String phone, String pass) {


//        查询手机号
        Student student = studentDao.selectByPhone(phone);
//        校验用户
        if (student != null) {
//            密文比较
            if (student.getPassword().equals(EncryptUtil.rsaEnc(prikey, pass))) {
                //当前账号在线 唯一登录  挤掉之前的登录
                if (RedissonUtil.checkKey("edu:login:phone:" + phone)) {
                    String oldtoken = (String) RedissonUtil.getStr("edu:login:phone:" + phone);
//                    新增挤掉信息
                    RedissonUtil.addStr("edu:login:jd:" + oldtoken, System.currentTimeMillis());
//                        删除原来的数据
                    RedissonUtil.delKey("edu:login:phone:" + phone, "edu:login:token:" + oldtoken);
                }
                //登录成功  -  生成令牌
                String token = JwtCore.createToken(phone);
//                存储到 Redis  唯一登录   一个账号只能在线一个  有效期 String
//                edu:login:phone:手机号 记录当前账号登录状态
                RedissonUtil.addStrTime("edu:login:phone:" + phone, token, 1800);
//                记录令牌对应的用户信息
                RedissonUtil.addStrTime("edu:login:token:" + token, student, 1800);
//                返回令牌
                return R.ok(token);
            }
        }

        return R.fail("登录失败!");
    }

    @Override
    public R upload(MultipartFile file) {

        if (!file.isEmpty()) {
//            获取上传的文件名
            String fn = file.getOriginalFilename();
//            获取上传的文件内容
            try {
                byte[] data = file.getBytes();
//                内容审核 鉴别图片是否合法
                if (ContentCensorUtil.censorImg(data)) {
//                对文件名进行重命名
                    fn = OssUtil.rename(fn);
//                将文件上传到阿里云的OSS
                    String url = OssUtil.upload(fn, data);
                    if (StringUtil.isnoEmpty(url)) {
                        return R.ok(url);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return R.fail();
    }

    @Override
    public R checkIdcrad(String token) {
//        令牌--用户id
        Student student = (Student) RedissonUtil.getStr("edu:login:token:" + token);
//        用户ID--数据库的实名认证资料
        Student studentreal = studentDao.selectByUserName(student.getUserName());
        if (studentreal.getFlag() == 2) {
            return R.fail("已经实名过了");
        } else {
//        实现图片识别
//        校验是否存在令牌
            String accessToken;
            if (RedissonUtil.checkKey("edu:baidu:tiken")) {
                accessToken = (String) RedissonUtil.getStr("edu:baidu:tiken");
            } else {
                accessToken = IdCardUtil.getToken();
                if (StringUtil.isnoEmpty(accessToken)) {
//               存储到Redis  有效期一个月
                    RedissonUtil.addStrTime("edu:baidu:tiken", accessToken, 29 * 24 * 60 * 60);
                } else {
                    return R.fail();
                }
            }
            String res = IdCardUtil.getIdcard(studentreal.getCardfront(), "front", accessToken);
            if (StringUtil.isnoEmpty(res)) {
//            校验结果
                if (studentreal.getName().equals(res)) {
//                一致  更改数据库
                    if (studentDao.updateReal(student.getUserName()) > 0) {
                        return R.ok();
                    }
                }
            }
            //        校验结果
            return R.fail();
        }
    }
}
