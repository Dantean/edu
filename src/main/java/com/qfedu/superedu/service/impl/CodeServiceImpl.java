package com.qfedu.superedu.service.impl;

import com.qfedu.superedu.common.core.CodeCreate;
import com.qfedu.superedu.common.model.CodeGenModel;
import com.qfedu.superedu.common.model.DbModel;
import com.qfedu.superedu.dto.CodeDto;
import com.qfedu.superedu.service.intf.CodeService;
import com.qfedu.superedu.vo.R;
import org.springframework.stereotype.Service;

@Service
public class CodeServiceImpl implements CodeService {
    @Override
    public R create(CodeDto dto) {
        DbModel dbModel=new DbModel(dto.getHost(),dto.getPort(),dto.getDbname(),dto.getUser(),dto.getPass());
        CodeGenModel codeGenModel=new CodeGenModel(dto.getPkname(),dto.getAuthor(),dto.getDes());
        if(CodeCreate.createCode(codeGenModel,dbModel)){
            return R.ok();
        }else {
            return R.fail();
        }
    }
}