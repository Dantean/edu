package com.qfedu.superedu.service.impl;

import com.github.pagehelper.PageHelper;
import com.qfedu.superedu.dao.HomeworkDao;
import com.qfedu.superedu.entity.Homework;
import com.qfedu.superedu.entity.HomeworkDate;
import com.qfedu.superedu.service.intf.HomeworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomeworkServiceImpl implements HomeworkService {

    @Autowired
    HomeworkDao homeworkDao;

//     添加作业的日期
    @Override
    public void addHomeWorkDate(String title) {
        homeworkDao.addHomeWorkDate(title);
    }

//     查看作业列表
    @Override
    public List<HomeworkDate> selectAll(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        return homeworkDao.selectAll();
    }

//    添加一个题目
    @Override
    public void addTopic(Homework homework) {
        homeworkDao.addTopic(homework);
    }

//    查看所有题目
    @Override
    public List<Homework> selectAllTopic(Integer uid, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        return homeworkDao.selectAllTopic(uid);
    }

    //    删除一条作业日期数据
    @Override
    public void deleteById(Integer id) {
        homeworkDao.deleteById(id);
    }

    //    删除一道题
    @Override
    public void deleteTopicById(Integer id) {
        homeworkDao.deleteTopicById(id);
    }
}
