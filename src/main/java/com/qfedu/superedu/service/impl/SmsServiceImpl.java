package com.qfedu.superedu.service.impl;

import com.qfedu.superedu.redis.RedissonUtil;
import com.qfedu.superedu.service.intf.SmsService;
import com.qfedu.superedu.utils.NumRandomUtil;
import com.qfedu.superedu.utils.RegistSmsUtil;
import com.qfedu.superedu.vo.R;
import org.springframework.stereotype.Service;

@Service
public class SmsServiceImpl implements SmsService {
    @Override
    public R SmsVer(String phone, int code) {
        if(RedissonUtil.checkKey("edu:"+phone)){
            if(code ==(Integer)RedissonUtil.getStr("edu:"+phone)){
                return R.ok();
            }else {
                return R.fail("验证码不正确");
            }
        }else {
            return R.fail("验证码失效或不存在");
        }
    }

    @Override
    public R SmsVer(String phone) {
        int code;
        if (RedissonUtil.checkKey("edu:" + phone)) {
//            原来的验证码没有失效
            code = (Integer) RedissonUtil.getStr("edu:" + phone);
        } else {
            code = NumRandomUtil.createNuum(6);
        }
        boolean falg = RegistSmsUtil.goSms(phone, code);
        if (falg) {
            RedissonUtil.addStrTime("edu:" + phone, code, 86400);

            return R.ok();
        }
        return R.fail();
    }
}
