package com.qfedu.superedu.service.impl;

import com.qfedu.superedu.dao.SignDao;
import com.qfedu.superedu.entity.Sign;
import com.qfedu.superedu.service.intf.SignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SignServiceImpl implements SignService {

    @Autowired
    private SignDao signDao;

    @Override
    public List<Sign> studentSign() {
        return signDao.studentSign();
    }
}
