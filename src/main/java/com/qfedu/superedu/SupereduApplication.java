package com.qfedu.superedu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
@MapperScan(basePackages = "com.qfedu.superedu.dao")   //设置dao的扫描
public class SupereduApplication {
    public static void main(String[] args) {
        SpringApplication.run(SupereduApplication.class, args);
    }
}