package com.qfedu.superedu.test;

import com.qfedu.superedu.utils.EncryptUtil;

import java.util.Map;

public class Pass_Main {
    public static void main(String[] args) {
        Map<String,String> map= EncryptUtil.createRSAKey();
        System.out.println(map.get(EncryptUtil.PUBKEY));
        System.out.println(map.get(EncryptUtil.PRIKEY));
        String pass="123456";
        String mw=EncryptUtil.rsaEnc(map.get(EncryptUtil.PRIKEY),pass);
        System.out.println("密文："+mw);
        System.out.println("解密："+EncryptUtil.rsaDec(map.get(EncryptUtil.PUBKEY),mw));
    }
}
