package com.qfedu.superedu.test;

import com.qfedu.superedu.common.core.CodeCreate;
import com.qfedu.superedu.common.model.DbModel;

public class GetTables_Main {
    public static void main(String[] args) {
        DbModel model=new DbModel();
        model.setDbname("education_sys");
        model.setHost("localhost");
        model.setPort(3306);
        model.setUser("root");
        model.setPass("123456");
        CodeCreate.parseTable(model);
    }
}
