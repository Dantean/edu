package com.qfedu.superedu.common;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

//全局的异常处理
//控制器的增强
@ControllerAdvice       // 该注解也需要扫描
@ResponseBody           //修饰类, 类中的所有方法, 相当于都是用该注解修饰
public class CommonException {

    //    处理那些没有预料到的异常
    @ExceptionHandler
    public JsonResult commonException(Exception ex) {
        return new JsonResult(0, ex.getMessage());
    }
}
