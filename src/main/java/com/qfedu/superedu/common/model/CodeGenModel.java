package com.qfedu.superedu.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: CodingsSart
 * @description:
 * @author: Feri(邢朋辉)
 * @create: 2020-09-14 09:46
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CodeGenModel {
   // private int uid;//登录用户
    private String pkname;//包名
    private String author;//作者
    private String des;//描述信息
}