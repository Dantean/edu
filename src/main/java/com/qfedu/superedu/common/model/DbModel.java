package com.qfedu.superedu.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: CodingsSart
 * @description: 数据库的信息
 * @author: Feri(邢朋辉)
 * @create: 2020-09-14 09:46
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DbModel {
    private String host;
    private int port;
    private String dbname;
    private String user;
    private String pass;
}
