package com.qfedu.superedu.api;

import com.qfedu.superedu.common.JsonResult;
import com.qfedu.superedu.entity.Student;
import com.qfedu.superedu.service.intf.SignService;
import com.qfedu.superedu.service.intf.StudentService;
import com.qfedu.superedu.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@Api(tags = "学生相关操作")
@RequestMapping("/student")
@RestController
public class StudentApi {
    @Autowired
    private SignService signService;
    @Autowired
    private StudentService studentService;

    @ApiOperation(value = "查询所有打卡记录", notes = "查询所有打卡记录")
    @GetMapping("/selectAllSign.do")
    public JsonResult selectAll() {
        return new JsonResult(1, signService.studentSign());
    }


    @ApiOperation(value = "查询用户名是否存在", notes = "查询用户名是否存在")
    @GetMapping("/selectByUserName.do")
    public JsonResult selectByUserName(String userName) {

        String s = studentService.selectByUserName(userName);
        return new JsonResult(1, s);
    }

    @ApiOperation(value = "验证手机号是否已被注册,不存在的话发送短信", notes = "验证手机号是否已被注册, 不存在的话发送短信")
    @GetMapping("/selectByPhone.do")
    public JsonResult selectByPhone(String phone) {
        System.out.println(phone);
        String s = studentService.selectByPhone(phone);
        return new JsonResult(1, s);
    }

    @ApiOperation(value = "学生注册", notes = "学生注册")
    @PostMapping("/studentRegist.do")
    public JsonResult studentRegist(Student student, String code) {
        return new JsonResult(1, studentService.regisStudent(student, code));
    }

    @ApiOperation(value = "上传实名照片", notes = "上传实名照片")
    @PostMapping("/cardupload.do")
    public R upload(MultipartFile file) {
        return studentService.upload(file);
    }

    @ApiOperation(value = "学生登录", notes = "学生登录")
    @PostMapping("/login.do")
    public R login(String phone, String pass) {
        return studentService.login(phone, pass);
    }

    @ApiOperation(value = "实名认证审核", notes = "实名认证审核")
    @GetMapping("checkrealcard.do")
    public R check(String token) {
        return studentService.checkIdcrad(token);
    }
}
