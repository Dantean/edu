package com.qfedu.superedu.api;

import com.qfedu.superedu.common.JsonResult;
import com.qfedu.superedu.entity.Homework;
import com.qfedu.superedu.service.intf.HomeworkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "作业相关操作")
@RequestMapping("/Homework")
@RestController
public class HomeworkApi {
    @Autowired
    HomeworkService homeworkService;

    @ApiOperation(value = "查看作业列表", notes = "查看作业列表")
    @GetMapping("/selectAll.do")
    public JsonResult selectAll(Integer page, Integer limit) {
        return new JsonResult(1, homeworkService.selectAll(page, limit));
    }

    @ApiOperation(value = "查看作业列表", notes = "查看作业列表")
    @PostMapping("/addHomeWorkDate.do")
    public JsonResult addHomeWorkDate(String title) {
        homeworkService.addHomeWorkDate(title);
        return new JsonResult(1, "请输入试题");
    }

    @ApiOperation(value = "添加一道题", notes = "添加一道题")
    @PostMapping("/addTopic.do")
    public JsonResult addTopic(Homework homework) {
        homeworkService.addTopic(homework);
        return new JsonResult(1, "添加成功");
    }

    @ApiOperation(value = "查看所有题目", notes = "查看所有题目")
    @PostMapping("/selectAllTopic.do")
    public JsonResult selectAllTopic(Integer uid, Integer page, Integer limit) {
        return new JsonResult(1, homeworkService.selectAllTopic(uid, page, limit));
    }

    @ApiOperation(value = "删除一条作业日期数据", notes = "删除一条作业日期数据")
    @PostMapping("/deleteById.do")
    public JsonResult deleteById(Integer id) {
        homeworkService.deleteById(id);
        return new JsonResult(1, "删除成功");
    }

    @ApiOperation(value = "删除一道题", notes = "删除一道题")
    @PostMapping("/deleteTopicById.do")
    public JsonResult deleteTopicById(Integer id) {
        homeworkService.deleteTopicById(id);
        return new JsonResult(1, "删除成功");
    }
}
