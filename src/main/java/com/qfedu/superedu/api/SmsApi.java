package com.qfedu.superedu.api;

import com.qfedu.superedu.service.intf.SmsService;
import com.qfedu.superedu.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/sms/")
@Api(tags = "短信的操作")

public class SmsApi {

    @Autowired
    private SmsService smsService;

    @GetMapping("sendcodesms.do")
    public R sendCode(String phone) {
        return smsService.SmsVer(phone);
    }

    @GetMapping("checkcodesms.do")
    public R checkCode(String phone, int code) {
        return smsService.SmsVer(phone, code);
    }
}
