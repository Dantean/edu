package com.qfedu.superedu.entity;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString
public class Video {
    private Integer id;
    private String name;
    private String vurl;
    private String iurl;
    private Integer tid;
    private String audit;
    private BigDecimal oprice;
    private BigDecimal nprice;
    private String flag;
    private Integer payCount;
}