package com.qfedu.superedu.entity;


import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Homework {
    private Integer id;
    private Integer hid;
    private String topic;
}
