package com.qfedu.superedu.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Question {
    private Integer id;
    private String name;
    private String optionA;
    private String optionB;
    private String optionC;
    private String optionD;
    private String answer;
    private Integer count;
    private Integer rightCount;

}
