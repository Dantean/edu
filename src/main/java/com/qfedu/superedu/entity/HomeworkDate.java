package com.qfedu.superedu.entity;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class HomeworkDate {
    private Integer id;
    private Date createDate;
    private String title;
}
