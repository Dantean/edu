package com.qfedu.superedu.entity;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class Teacher {
    private Integer id;
    private String userName;
    private String password;
    private String name;
    private Date birthday;
    private String phone;
    private String sex;
    private String graduationSchool;
    private String idCard;
    private String certificateCode;
    private Integer cid;
    private String birthArea;
    private String remark;
    private String imgpath;
    private Integer workyear;

}
