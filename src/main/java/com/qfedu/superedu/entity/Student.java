package com.qfedu.superedu.entity;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class Student {
    private String userName;
    private String password;
    private String name;
    private String phone;
    private Date birthday;
    private Integer score;
    private String sex;
    private String cardfront;   // 身份证正面
    private String cardrear;    //身份证反面
    private String cardhand;    //身份证手持
    private Integer flag;       //1.未实名 2.实名成功 3.实名失败
}
