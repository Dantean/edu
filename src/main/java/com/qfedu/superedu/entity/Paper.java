package com.qfedu.superedu.entity;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class Paper {
    private Integer id;
    private Integer tid;
    private String name;
    private Date createDate;

}
