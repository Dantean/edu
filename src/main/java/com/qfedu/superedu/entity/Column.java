package com.qfedu.superedu.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Column {
    private Integer id;
    private String name;
}
