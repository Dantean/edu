package com.qfedu.superedu.entity;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class Sign {
    private Integer id;
    private Date createTime;
    private Integer sid;
    private String content;

}
