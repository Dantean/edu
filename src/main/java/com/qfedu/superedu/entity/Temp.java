package com.qfedu.superedu.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Temp {
    private Integer id;
    private Integer pid;
    private Integer qid;
}
