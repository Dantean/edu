package com.qfedu.superedu.entity;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ToString
public class Order {
    private Integer id;
    private Date createTime;
    private Integer sid;
    private BigDecimal price;
    private Integer vid;
    private Integer tid;
    private String status;

}
