package com.qfedu.superedu.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Note {
    private Integer id;
    private Integer sid;
    private Integer cid;
    private String content;

}
