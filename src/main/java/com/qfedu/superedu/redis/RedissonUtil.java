package com.qfedu.superedu.redis;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.util.concurrent.TimeUnit;

public class RedissonUtil {
    private static String host = "106.14.9.62";
    private static int port = 6380;
    private static String pass = "nangong130197";
    private static RedissonClient client;

    static {
        Config config = new Config();
        config.useSingleServer().setPassword(pass).setAddress("redis://" + host + ":" + port);
        client = Redisson.create(config);
    }

    //新增
    public static void addStr(String key, Object val) {
        client.getBucket(key).set(val);
    }

    public static void addStrTime(String key, Object val, int seconds) {
        client.getBucket(key).set(val, seconds, TimeUnit.SECONDS);
    }

    //查询
    public static Object getStr(String key) {
        return client.getBucket(key).get();
    }

    //校验
    public static boolean checkKey(String key) {
        return client.getKeys().countExists(key) > 0;
    }

    //删除Key
    public static boolean delKey(String... key) {
        return client.getKeys().delete(key) > 0;
    }
}