package com.qfedu.superedu.dto;

import lombok.Data;

@Data
public class CodeDto {
    private String pkname;//包名
    private String author;//作者
    private String des;//描述信息
    private String host;
    private int port;
    private String dbname;
    private String user;
    private String pass;

}
