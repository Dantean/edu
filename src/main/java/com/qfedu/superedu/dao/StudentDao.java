package com.qfedu.superedu.dao;

import com.qfedu.superedu.entity.Student;
import com.qfedu.superedu.vo.R;
import org.springframework.stereotype.Component;

@Component
public interface StudentDao {
    Student selectByPhone(String phone);

    void regisStudent(Student student);

    Student selectByUserName(String userName);

    int updateReal(String userName);
}
