package com.qfedu.superedu.dao;

import com.qfedu.superedu.entity.Sign;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SignDao {
    List<Sign> studentSign();
}
