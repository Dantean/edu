package com.qfedu.superedu.dao;

import com.qfedu.superedu.entity.Homework;
import com.qfedu.superedu.entity.HomeworkDate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface HomeworkDao {
    //    添加作业的日期
    void addHomeWorkDate(String title);

    //    所有作业
    List<HomeworkDate> selectAll();

    //    添加一道题
    void addTopic(Homework homework);

    //    展示所有题目
    List<Homework> selectAllTopic(Integer uid);

    //    删除一条作业日期数据
    void deleteById(Integer id);

    //    删除一道题
    void deleteTopicById(Integer id);
}
